﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBehaviourPinto : BotBehaviour
{
     public float DistanceShow;
    
    // liste des states possibles pour ce comportement de bot
    public enum BotState{ 
        Scouting,
        Attack,
        Defend,
        GoToFlag,
    };
  
    // état actuel du bot
    public TeamBehaviourPinto teamBehaviour;
    public BotState state = BotState.Scouting;
    
    public int ScoutNum = 0;
    public Vector3 NMIpos;
    public int NMInear = 0;
    public string AttackSide;
    public bool haveFlag;
    public override void Init(GameMaster master, Bot bot)
    {
        base.Init(master, bot);
        this.teamBehaviour = FindObjectOfType<TeamBehaviourPinto>();
    }
    
    void Update()
    {
        Debug.Log("J'ai :" +teamBehaviour);
        if(!bot.agent.pathPending)
        {
            UpdateState();
        }
        if(Input.GetKeyDown(KeyCode.A))
        {
            NMIpos = enemyTeam.Places.GetPlacePosition(KeyPlaces.CAMPER);
            AttackSide = "TOP";
        }
        else if(Input.GetKeyDown(KeyCode.Z))
        {
            NMIpos = botTeam.Places.GetPlacePosition(KeyPlaces.CAMPER);
            AttackSide = "BOT";
        }
        else if(Input.GetKeyDown(KeyCode.E))
        {
            NMIpos = botTeam.Places.GetPlacePosition(KeyPlaces.CENTER);
            AttackSide = "CENTER";
        }
        if(Input.GetKeyDown(KeyCode.B))
        {
            NMInear = 0;
        }
    }
    
  
    // fonction appelée pour changer d'état
    public void SwitchState(BotState newState) {
        this.OnExitState();
        this.state = newState;
        this.OnEnterState();
    }

    protected void OnEnterState() {
        switch(state)
        {
            case BotState.Attack:
                    bot.agent.isStopped = false;
                    
                break;
            case BotState.GoToFlag:
                bot.agent.SetDestination(this.botTeam.Places.GetPlacePosition(KeyPlaces.FLAG));
                break;

        }
    }
    protected void UpdateState() {
        switch (state) {
            
            case BotState.Scouting:
                    switch(ScoutNum)
                {
                    case 0:
                        bot.agent.SetDestination(botTeam.Places.GetPlacePosition(KeyPlaces.POWER_UP));
                        StartCoroutine(Waiting());
                        if(NMIpos != new Vector3(0,0,0))
                        {
                            
                           DistanceShow = bot.agent.remainingDistance;
                            if(DistanceShow < 5)
                            {
                                bot.agent.isStopped = true;
                            }
                            else{
                                bot.agent.SetDestination(NMIpos);
                            }
                        }
                        //Si un Ennemi est proche
                            /*NMIpos = ennemi position
                            AttackSide = TOP;*/
                    break;
                    case 1:
                        bot.agent.SetDestination(botTeam.Places.GetPlacePosition(KeyPlaces.FRONT) - new Vector3(5,0,-5));
                        StartCoroutine(Waiting());
                        if(NMIpos != new Vector3(0,0,0) && state == BotState.Scouting)
                        {
                            DistanceShow = bot.agent.remainingDistance;
                            if(DistanceShow < 5)
                            {
                                bot.agent.isStopped = true;
                            }
                            else{
                                bot.agent.SetDestination(NMIpos);
                            }
                        }
                        //Si un Ennemi est proche
                            /*NMIpos = ennemi position
                            AttackSide = CENTER;*/
                    break;
                    case 2:
                        bot.agent.SetDestination(botTeam.Places.GetPlacePosition(KeyPlaces.FRONT)  - new Vector3(7,0,0));
                        StartCoroutine(Waiting());
                        if(NMIpos != new Vector3(0,0,0) && state == BotState.Scouting)
                        {
                            DistanceShow = bot.agent.remainingDistance;
                            if(DistanceShow < 5)
                            {
                                bot.agent.isStopped = true;
                            }
                            else{
                                bot.agent.SetDestination(NMIpos);
                            }
                        }
                        //Si un Ennemi est proche
                            /*NMIpos = ennemi position
                            AttackSide = CENTER;*/
                    break;
                    case 3:
                        bot.agent.SetDestination(botTeam.Places.GetPlacePosition(KeyPlaces.FRONT) - new Vector3(5,0,5));
                        StartCoroutine(Waiting());
                        if(NMIpos != new Vector3(0,0,0) && state == BotState.Scouting)
                        {
                            DistanceShow = bot.agent.remainingDistance;
                            if(DistanceShow < 5)
                            {
                                bot.agent.isStopped = true;
                            }
                            else{
                                bot.agent.SetDestination(NMIpos);
                            }
                        }
                        //Si un Ennemi est proche
                            /*NMIpos = ennemi position
                            AttackSide = CENTER;*/
                    break;
                    case 4:
                        bot.agent.SetDestination(botTeam.Places.GetPlacePosition(KeyPlaces.CAMPER));
                        StartCoroutine(Waiting());
                        if(NMIpos != new Vector3(0,0,0) && state == BotState.Scouting)
                        {
                            DistanceShow = bot.agent.remainingDistance;
                            if(DistanceShow < 5)
                            {
                                bot.agent.isStopped = true;
                            }
                            else{
                                bot.agent.SetDestination(NMIpos);
                            }
                        }
                        //Si un Ennemi est proche
                            /*NMIpos = ennemi position
                            AttackSide = BOT;*/
                    break;
                }
                break;
            
            case BotState.Attack: 
                switch(AttackSide)
                    {
                        case "CENTER":
                            bot.agent.SetDestination(enemyTeam.Places.GetPlacePosition(KeyPlaces.FLAG));
                         break;
                        case "TOP":
                            bot.agent.SetDestination(enemyTeam.Places.GetPlacePosition(KeyPlaces.SPAWN));
                        break;
                        case "BOT" :
                            bot.agent.SetDestination(enemyTeam.Places.GetPlacePosition(KeyPlaces.PYLON));
                        break;
                        default:
                            bot.agent.SetDestination(enemyTeam.Places.GetPlacePosition(KeyPlaces.FLAG));
                        break;
                    }
                    if(bot.agent.isStopped == true && AttackSide != "CENTER")
                    {
                        bot.agent.SetDestination(enemyTeam.Places.GetPlacePosition(KeyPlaces.FLAG));
                    }
                    DistanceShow = this.enemyTeam.Places.GetPlaceDistance(this.transform.position, KeyPlaces.FLAG);
                    if(DistanceShow < 1f)
                    {
                        haveFlag = true;
                    }
               if(haveFlag == true)
               {
                    Debug.Log("On a le drapeau");
                    BotBehaviourPinto[] BBP = GameObject.FindObjectsOfType<BotBehaviourPinto>();
                    foreach(BotBehaviourPinto B in BBP)
                    {
                        B.SwitchState(BotState.GoToFlag);
                    }        
               }
                
            break;
            case BotState.GoToFlag:
                bot.agent.SetDestination(botTeam.Places.GetPlacePosition(KeyPlaces.FLAG));
                DistanceShow = bot.agent.remainingDistance;
                if(DistanceShow < 1f)
                {
                    haveFlag = false;
                    
                    SwitchState(BotState.Scouting);
                }
            break;
        }
        
    }

    private void Update()
    {
        if (this.bot.visibleEnemyBots.Count > 0)
        {
            var direction = this.bot.visibleEnemyBots[0].transform.position - this.bot.transform.position;
            this.bot.ShootInDirection(direction);
        }
        if(this.botTeam.)
    }

    protected void OnExitState() {
        switch(state)
        {
            case BotState.Defend:
                break;
            case BotState.Scouting:
            break;

        }
    }
    
    private void OnValidate() {
        this.OnEnterState();
    }

    IEnumerator Waiting()
    {
        yield return new WaitForSeconds(5);
        if(NMInear == 0)
            SwitchState(BotState.Attack);
        Debug.Log("Attaque !");
    }

}
