﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TeamBehaviourPinto : TeamBehaviour
{
    public BotBehaviourPinto Leader;
    public BotBehaviourPinto[] myBots; // pour éviter d'avoir à chercher les components qui ont le code spécifique de mes bots
    public override void RegisterBot(Bot[] bots) {
        //Leader = GameObject.FindGameObjectWithTag("Leader").GetComponent<BotBehaviourPinto>();
        this.myBots = new BotBehaviourPinto[bots.Length];
        for (int i = 0; i < this.myBots.Length; i++) {
          this.myBots[i] = bots[i].GetComponent<BotBehaviourPinto>();
        }
  }

  public override void OnMatchStart() {
    for(int i = 0; i< myBots.Length; i++)
    {
      myBots[i].ScoutNum = i;
    }
  }

}
